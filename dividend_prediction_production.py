# -*- coding: utf-8 -*-
"""
Created on Wed Jan 23 9:20 2015
Updated on Wed April 29 9:20 2015

@author Yiduo Tang tyiduo@gmail.com

Estimate the dividends between the expirations
Assumptions:
1. We run the program in the morning before the market opens
2. There will be at most one dividend each year per company

Better structured version, supportts more indexes (IH, IC...)

"""
from __future__ import division, print_function
import pymssql
import datetime as dt
import numpy as np
import pandas as pd
import calendar
import pdb

def inner_code_map(index):
    code_map = {
    "IF":3145,
    "IC":4978,
    "IH":46}
    return code_map[index]

def next_this_date(date, month, day):
    """Returns the next occurence of month-day after the input date. If it is 2-29, we will try both 
    2-29 and 2-28, whichever comes first"""

    year = date.year
    if month == 2 and day == 29 and not calendar.isleap(year):
        try_date = dt.date(year, month, 28)
    else:
        try_date = dt.date(year, month, day)
    if try_date > date:
        return try_date
    elif month == 2 and day == 29 and not calendar.isleap(year+1):
        return dt.date(year+1, month, 28)
    else:
        return dt.date(year+1, month, day)


def futures_contracts_and_expirations(index, date, conn):
    """This function will fetch the four active IF contracts and their expirations
    parameters
    ----------
    index: the suffix of the index futures, such as IF, IC and IH
    date: the date we are analysing
    conn: the pymssql connection object

    returns
    -------
    two lists, the first for contracts and the second for expirations
    """

    query = """
    SELECT TOP 1 [ContractCode], [LastTradingDate]
      FROM [JYDB].[dbo].[Fut_ContractMain]
    WHERE ContractCode Like '%s%%' and LastTradingDate >= '%s'
    ORDER BY ContractCode
    """%(index, date)
    result = pd.read_sql(query, conn)
    contracts = []
    expirations = []
    contracts.append(result["ContractCode"][0])
    expirations.append(result["LastTradingDate"][0].date())
    year = int(contracts[0][2:4])
    month = int(contracts[0][4:6])
    if month%3 == 1:
        months = (month+1, (month+2)%12, (month+5)%12)
    elif month%3 == 2:
        months = (month+1, (month+4)%12, (month+7)%12)
    else:
        months = ((month+1)%12, (month+3)%12, (month+6)%12)
    for m in months:
        if m == 0:
            m = 12
        if m > month:
            contracts.append(u"%s%s%02d"%(index, year, m))
        else:
            contracts.append(u"%s%s%02d"%(index, year+1, m))
    for contract in contracts[1:]:
        query = """
        SELECT [LastTradingDate]
          FROM [JYDB].[dbo].[Fut_ContractMain]
        WHERE ContractCode = '%s'
        """%contract
        result = pd.read_sql(query, conn)
        expirations.append(result["LastTradingDate"][0].date())
    return contracts, expirations


def index_dividends_prediction(index, date, conn, ref_l=5, min_waiting_day=26, min_range=1, expirations=None, info_output=False):
    """This is the function to make predictions on effect on basis from dividends of index components

    Parameters
    ---------- 
    index: the suffix of the index futures, such as IF, IC and IH
    date: the date we make predictions
    conn: the SQLServer connection object
    ref_l: the number of years to look back for reference
    min_waiting_day: the least number of days between ExDiviDate and AdvanceDate
    min_range: the smallest length of our prediction period
    expirations: the expirations between which we want to fill the dvidends
    info_output: Whether to output detailed information for debugging

    Returns
    -------
    dividends: a 3-row table where the first row is the dividends for sure between each two consecutive 
    expirations, the second row for those we only predict time, the third row for those we predict both amount
    and time of the dividends.
    """

    #If there is no input expirations, we fetch expirations of the four active contract
    
    log_directory = "D:\\HangHe"#The directory to put the log
    
    if expirations is None:
         contracts, expirations = futures_contracts_and_expirations(index, date, conn)
         n_exp = 4 #number of expirations
    else:
        n_exp = len(expirations) 
    #Get the last time when weights are updated
    query = """
    SELECT MAX([EndDate])
    FROM [JYDB].[dbo].[LC_IndexComponentsWeight]
    WHERE [IndexCode] = %s AND [EndDate] < '%s'
    """%(inner_code_map(index), date)
    last_update_date = pd.read_sql(query, conn).ix[0, 0]

    #Get the last trading day
    #Issues: what if one of component of the list cannot be traded on that day
    query = """
    SELECT MAX([TradingDay])
    FROM [JYDB].[dbo].[QT_DailyQuote]
    WHERE [TradingDay] < '%s'
    """%date
    last_trading_date = pd.read_sql(query, conn).ix[0, 0]

    #Get the tickers and weights of the latest update
    query = """
    SELECT Wgt.[InnerCode], Wgt.[Weight]/100 as Weight, [ClosePrice]
    FROM [JYDB].[dbo].[LC_IndexComponentsWeight] as Wgt
    JOIN [JYDB].[dbo].[QT_DailyQuote] as Quote
    ON Wgt.[InnerCode] = Quote.[InnerCode] and Quote.[TradingDay] = '%s'
    WHERE Wgt.[IndexCode] = %s AND Wgt.[EndDate] = '%s'
    ORDER By Wgt.[InnerCode]
    """%(last_trading_date, inner_code_map(index), last_update_date)
    weights = pd.read_sql(query, conn)

    #Fetch the dividend information
    query = """
    SELECT Wgt.[InnerCode], Div.[EndDate], Div.[EventProcedure], Div.[IfDividend],
             Div.[AdvanceDate], Div.[SMDeciPublDate], Div.[DividendImplementDate],
             Div.[CashDiviRMB]/10 as CashDiviRMB, Div.[ExDiviDate], 
             Wgt.[Weight]/100 as Weight, Quote.[ClosePrice]
      FROM [JYDB].[dbo].[LC_IndexComponentsWeight] as Wgt
      LEFT JOIN [JYDB].[dbo].[LC_Dividend] as Div
      ON Wgt.[InnerCode] = Div.[InnerCode] and Wgt.[EndDate] = '%s' and Wgt.[IndexCode] = %s
      LEFT JOIN [JYDB].[dbo].[QT_DailyQuote] as Quote
      ON Wgt.[InnerCode] = Quote.[InnerCode] and Quote.[TradingDay] = '%s'
      WHERE (Div.[AdvanceDate] < '%s' or Div.[SMDeciPublDate] < '%s') and Div.[EndDate] > '%s'
    ORDER BY Div.InnerCode, Div.EndDate DESC
    """%(last_update_date, inner_code_map(index), last_trading_date, date, date, date - dt.timedelta(days=365*(ref_l+1)))

    diviInfo = pd.read_sql(query, conn)
    diviInfoGroup = diviInfo.groupby("InnerCode")
    keys = sorted(diviInfoGroup.groups.keys())
    dividends = np.zeros((3, n_exp)) #Here we store the temporate information, the first column for the dividend between
    #the input date and the first expiration, the second column for the dividend between the first two expirations
    #and so on. The first row is for those with both date and amount announced, second row for those only has
    #amount announced, third row those haven't announced yet
    p_none = []#Variables for debugging
    p_date = []
    p_both = []

    for h, inner_code in enumerate(keys):
        index = diviInfoGroup.groups.get(inner_code, [])
        #ISSUES: some times some inner code in weights cannot be found in diviInfo, though such cases are rare
        pred_date = False#whether we should make predictions only for ExDiviDate
        pred_both = False#whether we should make predictions for both the amount of dividends and the date
        #We scan the dividend table to see what we know about the next dividend
        i = index[0]
        if np.isnan(diviInfo["ClosePrice"][i]):
            query = """
            SELECT TOP 1 [ClosePrice]
            FROM [JYDB].[dbo].[QT_DailyQuote]
            WHERE [InnerCode] = %s and [TradingDay] < '%s'
            ORDER BY [TradingDay] DESC
            """%(inner_code, date)
            ClosePrice = pd.read_sql(query, conn).ix[0, 0]
        else:
            ClosePrice = diviInfo["ClosePrice"][i]

        #pdb.set_trace()
        if (diviInfo["ExDiviDate"][i] not in (pd.NaT, None)
            and diviInfo["ExDiviDate"][i].date() >= date and 
            diviInfo["DividendImplementDate"][i] not in (pd.NaT, None) and 
            diviInfo["DividendImplementDate"][i].date() < date and 
            not np.isnan(diviInfo["CashDiviRMB"][i]) and diviInfo["IfDividend"][i] == 1):
            #The case where we know both amount and date of the dividend
            for j in range(n_exp):
                if diviInfo["ExDiviDate"][i].date() <= expirations[j]:
                    dividends[0, j] += diviInfo["CashDiviRMB"][i]/ClosePrice * diviInfo["Weight"][i]
                    break
                else:
                    pass
            if info_output:
                p_none.append([inner_code, diviInfo["DividendImplementDate"][i], diviInfo["ExDiviDate"][i]] + list(dividends[0, :]))
        
        elif (((diviInfo["AdvanceDate"][i] not in (pd.NaT, None) and diviInfo["AdvanceDate"][i].date() < date 
                and diviInfo["AdvanceDate"][i].year == date.year) 
            or (diviInfo["SMDeciPublDate"][i] not in (pd.NaT, None) and diviInfo["SMDeciPublDate"][i].date() 
                < date and diviInfo["SMDeciPublDate"][i].year == date.year))
            and diviInfo["IfDividend"][i] == 0 and date.month > 6 and expirations[3].month != 6):
            #The case where we know for sure that this year will not have dividends
            pass

        elif (((diviInfo["AdvanceDate"][i] not in (pd.NaT, None) and diviInfo["AdvanceDate"][i].date() < date 
                and diviInfo["AdvanceDate"][i].year == date.year) 
            or (diviInfo["SMDeciPublDate"][i] not in (pd.NaT, None) and diviInfo["SMDeciPublDate"][i].date() 
                < date and diviInfo["SMDeciPublDate"][i].year == date.year))
            and (diviInfo["DividendImplementDate"][i] in (None, pd.NaT) or diviInfo["DividendImplementDate"][i].date() >= date)
            and diviInfo["IfDividend"][i] == 1 and
            not np.isnan(diviInfo["CashDiviRMB"][i])):
            #If we know only the amount but not the date
            if diviInfo["SMDeciPublDate"][i] not in (pd.NaT, None) and diviInfo["SMDeciPublDate"][i].date() < date:
                ref_date = diviInfo["SMDeciPublDate"][i].date()
            else:
                ref_date = diviInfo["AdvanceDate"][i].date()
            year = ref_date.year
            ExDates = []
            dividend = diviInfo["CashDiviRMB"][i] / ClosePrice * diviInfo["Weight"][i]
            pred_date = True
      
            for j in range(i+1, index[-1]+1):
                if (diviInfo["EventProcedure"][j] == 3131 
                    and diviInfo["IfDividend"][j] == 1
                    and diviInfo["AdvanceDate"][j] not in (pd.NaT, None)
                    and year - diviInfo["AdvanceDate"][j].year <= ref_l
                    and diviInfo["ExDiviDate"][j] not in (pd.NaT, None)
                    and diviInfo["ExDiviDate"][j].date() < date):
                    t_month = diviInfo["ExDiviDate"][j].date().month
                    t_day = diviInfo["ExDiviDate"][j].date().day
                    date_cand = next_this_date(dt.date(year, 1, 1), t_month, t_day)
                    ExDates.append(date_cand)
                elif year - diviInfo["AdvanceDate"][j].year > ref_l:
                    break
                else:
                    pass
        
        else:
        #If we don't know either the amount or date
            pred_both = True
            year = date.year
            ExDates = []
            ExDividends = []
            this_year = False

            for j in range(i, index[-1]+1):
                if (diviInfo["EventProcedure"][j] == 3131 
                    and not np.isnan(diviInfo["CashDiviRMB"][j])
                    and diviInfo["IfDividend"][j] == 1
                    and diviInfo["AdvanceDate"][j] not in (pd.NaT, None)
                    and year - diviInfo["AdvanceDate"][j].year <= ref_l
                    and diviInfo["ExDiviDate"][j] not in (pd.NaT, None)
                    and diviInfo["ExDiviDate"][j].date() < date):
                    t_month = diviInfo["ExDiviDate"][j].date().month
                    t_day = diviInfo["ExDiviDate"][j].date().day
                    if diviInfo["ExDiviDate"][j].year == date.year:
                        this_year = True
                    else:
                        pass
                    if this_year:
                        ExDates.append(next_this_date(dt.date(year+1, 1, 1), t_month, t_day))
                    else:
                        ExDates.append(next_this_date(dt.date(year, 1, 1), t_month, t_day))
                    ExDividends.append(diviInfo["CashDiviRMB"][j]/ClosePrice)
                elif year - diviInfo["AdvanceDate"][j].year > ref_l:
                    break
                else:
                    pass
            if len(ExDividends) >= 2 and len(ExDividends) >= ref_l:
                dividend = np.mean(ExDividends) * diviInfo["Weight"][i]
            elif len(ExDividends) >= 2 and len(ExDividends) < ref_l:
                dividend = np.sum(ExDividends)/ref_l * diviInfo["Weight"][i] 
                #If some years do not have dividend, we 'dilute' those years that have dividend
            else:
                dividend = 0

        #pdb.set_trace()
        if pred_date or pred_both:
            if pred_date:
                row = 1
            else:
                row = 2
            
            #We assume the dividend day falls between first_psd(first possible date) and last_psd with uniform
            #probability
            if len(ExDates) >= 2:
                first_psd = min(ExDates)
                last_psd = max(ExDates)
            else:
                first_psd = dt.date(1900, 1, 1)
                last_psd = dt.date(year, 8, 30)
            if pred_date:
                first_psd = max(ref_date + dt.timedelta(days = min_waiting_day), first_psd)
            else:
                first_psd = max(date + dt.timedelta(days = min_waiting_day), first_psd)
            first_psd = max(first_psd, date + dt.timedelta(days=5))
            last_psd = max(first_psd + dt.timedelta(min_range), last_psd)
            ld = (last_psd - first_psd).days + 1
            ls = np.zeros(n_exp) #The distribution of the possible dividend dates between the expirations, eg, 
            #if there are 4 possible dates between expiration1 and expiration2, then ls[1] = 4
            if first_psd <= expirations[0]:
                if last_psd <= expirations[0]:
                    ls[0] = ld
                else:
                    ls[0] = (expirations[0] - first_psd).days + 1
            else:
                pass
            for i in range(n_exp-1):
                head = max(expirations[i] + dt.timedelta(days = 1), first_psd)
                tail = min(expirations[i+1], last_psd)
                if tail >= head:
                    ls[i+1] = (tail - head).days + 1
            ls = ls/ld
            dividends[row, :] += ls * dividend
            if info_output:
                i = index[0]
                if pred_date:
                    p_date.append([inner_code, first_psd, last_psd, diviInfo["DividendImplementDate"][i], diviInfo["ExDiviDate"][i]] + list(ls) + list(ls*dividend))
                else:
                    p_both.append([inner_code, first_psd, last_psd, diviInfo["DividendImplementDate"][i], diviInfo["ExDiviDate"][i]] + list(ls) + list(ls*dividend))
            #pdb.set_trace()
    if info_output:
        l = len(expirations)
        if len(p_none) > 0:
            pd.DataFrame(data=p_none, columns=["InnerCode", "Impl Date", "Ex Date"]+range(1, l+1)).to_csv(log_directory+"\\%s p_none%s.csv"%(date, min_range))
        if len(p_date) > 0:
            pd.DataFrame(data=p_date, columns=["InnerCode", "FirstDate", "LastDate", "Impl Date", "Ex Date"]+range(1, l+1)+range(1, l+1) ).to_csv(log_directory+"\\%s p_date%s.csv"%(date, min_range))
        if len(p_both) > 0:
            pd.DataFrame(data=p_both, columns=["InnerCode", "FirstDate", "LastDate", "Impl Date", "Ex Date"]+range(1, l+1)+range(1, l+1) ).to_csv(log_directory+"\\%s p_both%s.csv"%(date, min_range))
    return dividends


def future_basis(index, begin, end, conn):
    """return the data frame that contains the basis between IF contracts. The date range is 
    from begin to end (inclusive)"""

    query = """
    SELECT Fut.TradingDay, Fut.ContractCode, Fut.ClosePrice, Cash.ClosePrice AS Cash
    FROM [JYDB].[dbo].[Fut_TradingQuote]as Fut
    LEFT JOIN [JYDB].[dbo].[QT_DailyQuote] as Cash
    ON Fut.TradingDay = Cash.TradingDay and Cash.InnerCode = %s
    WHERE ContractCode Like '%s%%' and Fut.TradingDay >= '%s' and Fut.TradingDay <= '%s'
    ORDER BY TradingDay, ContractCode
    """%(inner_code_map(index), index, begin, end)
    
    result = pd.read_sql(query, conn)
    fut_price = np.array(result["ClosePrice"])
    l = len(fut_price)
    fut_price = fut_price.reshape((l/4, 4))
    cash_price = np.array(result["Cash"])
    cash_price = cash_price[::4]
    basis = np.zeros((l/4, 4))
    basis[:, 0] = fut_price[:, 0] - cash_price
    for i in range(1, 4):
        basis[:, i] = fut_price[:, i] - fut_price[:, i-1]                 ####################################################### why fut_price - fut_price
    index = result["TradingDay"][::4]
    return pd.DataFrame(index=index, data=basis)


def real_dividends(index, date, conn, expirations=None):
    """The DEPRECATED version to calculate the realized dividends' effect on basis when we look back"""
    #Now we get the last date of weight update
    query = """
    SELECT MAX([EndDate])
    FROM [JYDB].[dbo].[LC_IndexComponentsWeight]
    WHERE [IndexCode] = %s AND [EndDate] <= '%s'
    """%(inner_code_map(index), date)
    last_update_date = pd.read_sql(query, conn).ix[0, 0]

    if expirations is None:
        contracts, expirations = futures_contracts_and_expirations(date, conn)
    
    dates = [date] + list(expirations)
    dividends = np.zeros(4)
    for i in range(4):
        query = """
        SELECT Div.[InnerCode], Div.[EndDate], Div.[EventProcedure], Div.[IfDividend], 
            Div.[CashDiviRMB]/10 as CashDiviRMB, Div.[RightRegDate], Div.[ExDiviDate], 
            Wgt.[Weight]/100 as Weight, Quote.[ClosePrice]
        FROM [JYDB].[dbo].[LC_IndexComponentsWeight] as Wgt
        JOIN [JYDB].[dbo].[LC_Dividend] as Div
        ON Wgt.[InnerCode]=Div.[InnerCode] and Wgt.[EndDate]='%s' and Wgt.[IndexCode]=%s
        JOIN [JYDB].[dbo].[QT_DailyQuote] as Quote
        ON Quote.[TradingDay] = Div.[RightRegDate] and Quote.[InnerCode] = Div.[InnerCode]
        WHERE Div.[EventProcedure] = 3131 and Div.[IfDividend] = 1 and Div.[CashDiviRMB] is not null and
        Div.[ExDiviDate] > '%s' and Div.[ExDiviDate] <= '%s'
        """%(last_update_date, inner_code_map(index), dates[i], dates[i+1])
        diviInfo = pd.read_sql(query, conn)
        l = len(diviInfo)
        for j in range(l):
            #Then we get the tickers and weights of the latest update
            query = """
            SELECT Wgt.[InnerCode], Wgt.[Weight]/100 as Weight, [ClosePrice]
            FROM [JYDB].[dbo].[LC_IndexComponentsWeight] as Wgt
            JOIN [JYDB].[dbo].[QT_DailyQuote] as Quote
            ON Wgt.[InnerCode] = Quote.[InnerCode] and Quote.[TradingDay] = '%s'
            WHERE Wgt.[IndexCode] = %s AND Wgt.[EndDate] = '%s'
            ORDER By Wgt.[InnerCode]
            """%(inner_code_map(index), diviInfo.ix[j, "RightRegDate"], last_update_date)
            #weights = pd.read_sql(query, conn)

            dividends[i] += diviInfo["CashDiviRMB"][j] * diviInfo["Weight"][j]
    #pdb.set_trace()
    return dividends


def index_dividends_distribution(index, start, end, conn):
    """The distribution of dividends on CSI300 through the calendar"""

    #We first get all the CSIs
        
    query = """
    SELECT TradingDay, ClosePrice
    FROM [JYDB].[dbo].[QT_DailyQuote]
    WHERE InnerCode = %s and TradingDay >= '%s' and TradingDay <= '%s'
    ORDER BY TradingDay
    """%(inner_code_map(index), start, end)
    CSI300 = pd.read_sql(query, conn)
    dividends = np.zeros(len(CSI300))
    
    for i, date in enumerate(CSI300["TradingDay"]):
        query = """
        SELECT MAX([EndDate])
        FROM [JYDB].[dbo].[LC_IndexComponentsWeight]
        WHERE [IndexCode] = %s AND [EndDate] <= '%s'
        """%(inner_code_map(index), date)
        last_update_date = pd.read_sql(query, conn).ix[0, 0]

        query = """
        SELECT Div.InnerCode, Div.CashDiviRMB/10 as CashDiviRMB, Quote.ClosePrice, Wgt.Weight/100 AS Weight
        FROM [JYDB].[dbo].[LC_Dividend] as Div
        JOIN [JYDB].[dbo].[LC_IndexComponentsWeight] as Wgt
        ON Wgt.[InnerCode] = Div.[InnerCode] and Wgt.[EndDate] = '%s' and Wgt.[IndexCode] = %s
        JOIN [JYDB].[dbo].[QT_DailyQuote] as Quote
        ON Quote.[InnerCode] = Div.[InnerCode] and Quote.[TradingDay] = '%s'
        WHERE Div.[RightRegDate] = '%s' and Div.[IfDividend] = 1 and Div.CashDiviRMB is not null
        """%(last_update_date, inner_code_map(index), date, date)
        diviInfo = pd.read_sql(query, conn)

        query = """
        SELECT Wgt.[InnerCode], Wgt.[Weight]/100 as Weight, [ClosePrice]
        FROM [JYDB].[dbo].[LC_IndexComponentsWeight] as Wgt
        JOIN [JYDB].[dbo].[QT_DailyQuote] as Quote
        ON Wgt.[InnerCode] = Quote.[InnerCode] and Quote.[TradingDay] = '%s'
        WHERE Wgt.[IndexCode] = %s AND Wgt.[EndDate] = '%s'
        ORDER By Wgt.[InnerCode]
        """%(date, inner_code_map(index), last_update_date)
        weights = pd.read_sql(query, conn)

        dividends[i] += sum(diviInfo["CashDiviRMB"] * diviInfo["Weight"] / diviInfo["ClosePrice"])

    return pd.DataFrame(index = CSI300["TradingDay"], data=dividends, columns=["Dividend"])


def index_dividends_between_expirations(index, start, end, conn):
    lc, le = futures_contracts_and_expirations(end, conn)
    last_expiration = le[-1]
    dividends = index_dividends_distribution(start, last_expiration, conn)
    contracts, expirations = futures_contracts_and_expirations(start, conn)
    query = """
    SELECT TradingDay, ClosePrice
    FROM [JYDB].[dbo].[QT_DailyQuote]
    WHERE InnerCode = %s and TradingDay >= '%s' and TradingDay <= '%s'
    ORDER BY TradingDay
    """%(inner_code_map(index), start, end)
    CSI300 = pd.read_sql(query, conn)
    diviExp = np.zeros((len(CSI300), 4))

    for i, date in enumerate(dividends.index):

        if date.date() > end:
            break
            

        elif date.date() > expirations[0]:
            contracts, expirations = futures_contracts_and_expirations(date.date(), conn)

        diviExp[i, 0] = np.sum(dividends[date:expirations[0]])
        for j in range(1, 4):
            diviExp[i, j] = np.sum(dividends[expirations[j-1]:expirations[j]])
        
    return diviExp


def index_dividends_between_months(index, start, end, conn):
    """
    Issues
    ------
    We use information about IF to get the expirations, because IC and IH have a history
    which is too short
    """

    lc, le = futures_contracts_and_expirations(end, conn)
    last_expiration = le[-1]
    dividends = index_dividends_distribution(start, last_expiration, conn)
    query = """
    SELECT [ContractCode], [LastTradingDate]
    FROM [JYDB].[dbo].[Fut_ContractMain]
    WHERE ContractCode Like 'IF%s__'
    ORDER BY ContractCode
    """%(str(start.year)[-2:])
    result = pd.read_sql(query, conn)
    expirations = [date.date() for date in result["LastTradingDate"]]

    query = """
    SELECT TradingDay, ClosePrice
    FROM [JYDB].[dbo].[QT_DailyQuote]
    WHERE InnerCode = %s and TradingDay >= '%s' and TradingDay <= '%s'
    ORDER BY TradingDay
    """%(inner_code_map(index), start, end)
    CSI300 = pd.read_sql(query, conn)
    diviExp = np.zeros((len(CSI300), 12))

    for i, date in enumerate(dividends.index):

        if date.date() > end:
            break

        elif date.year > expirations[0].year:
            query = """
            SELECT [ContractCode], [LastTradingDate]
            FROM [JYDB].[dbo].[Fut_ContractMain]
            WHERE ContractCode Like 'IF%s__'
            ORDER BY ContractCode
            """%(str(date.year)[-2:])
            result = pd.read_sql(query, conn)
            expirations = [date.date() for date in result["LastTradingDate"]]

        for j in range(1, 12):
            diviExp[i, j] = np.sum(dividends[date.date():expirations[j]])
        
    return diviExp
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 20 9:20 2015

@author Yiduo Tang tyiduo@gmail.com

Adjusting the basis of CSI 300 index futures for the expected dividends and out put to a csv file

"""

import pymssql
import datetime as dt
import numpy as np
import pandas as pd
import unicodecsv
import calendar
import pdb
import os
import sys
import pylab as plt
import seaborn as sb


def make_report(date, conn):
    """Make a report of IF term structure on most recent two trading days

    Parameters
    ----------
    date: the date when we are making the calculation, or when we assume we were

    Returns
    -------
    output: a dictionary containing pandas DataFrame which consists of basis, dividend adjustment, 
        and adjusted basis for most recent two trading days
    """

    output = {}
    #We first get the price information for both CSI 300 and active IF futures
    for ticker in ("IF", "IC", "IH"):
        tickers=ticker+"1"
        query = """
        SELECT Top 8 Fut.TradingDay, Fut.ContractCode, Fut.ClosePrice, Cash.ClosePrice AS Cash
        FROM [JYDB].[dbo].[Fut_TradingQuote]as Fut
        LEFT JOIN [JYDB].[dbo].[QT_DailyQuote] as Cash
        ON Fut.TradingDay = Cash.TradingDay and Cash.InnerCode = %s
        WHERE ContractCode Like '%s%%' and Fut.TradingDay < '%s'
        ORDER BY TradingDay DESC, ContractCode
        """%(inner_code_map(ticker), tickers, date)
        result = pd.read_sql(query, conn)
        fut_price = np.array(result["ClosePrice"])
        l = len(fut_price)
        assert(l==8)
        fut_price.shape = (l/4, 4)
        cash_price = np.array(result["Cash"])
        cash_price = cash_price[::4]
        index = [result["TradingDay"][0].date(), result["TradingDay"][4].date()]
        #Then we get the contracts and expirations on the two most recent trading days
        contracts1, expirations1 = futures_contracts_and_expirations(ticker, index[0], conn)
        contracts2, expirations2 = futures_contracts_and_expirations(ticker, index[1], conn)
        query = """
        SELECT MAX([TradingDay])
        FROM [JYDB].[dbo].[QT_DailyQuote]
        WHERE [TradingDay] < '%s'
        """%date
        last_trading_date = pd.read_sql(query, conn).ix[0, 0].date()
        dividends = index_dividends_prediction(ticker, last_trading_date, conn)
        dividends = sum(dividends)
        dividends = np.cumsum(dividends)
        
        if contracts1[0] == contracts2[0]:
            dividends_adj = cash_price.reshape((2, 1)) * dividends
            fut_price_out = fut_price
        elif contracts1[0] == contracts2[1]:
        # 
            dividends_adj = np.zeros((2, 4))
            fut_price_out = np.zeros((2, 4))
            dividends_adj[0, :] = cash_price[0] * dividends
            fut_price_out[0, :] = fut_price[0, :]
            for i in range(4):
                for j in range(4):
                    if contracts1[i] == contracts2[j]:
                        fut_price_out[1, i] = fut_price[1, j]
                        dividends_adj[1, i] = cash_price[1] * dividends[i]
                    else:
                        pass
            #dividends_adj[dividends_adj == 0] = None
            fut_price_out[fut_price_out == 0 ] = None

        else:
            raise Exception("Something is wrong with the contracts")

        basis = np.hstack((cash_price.reshape((2, 1)), fut_price_out))
        dividends_adj = np.hstack((np.zeros((2, 1)), dividends_adj))
        print("Dividends for %s is:"%ticker)
        print(dividends_adj)
        adjusted_basis = basis+dividends_adj
        adjusted_basis /= adjusted_basis[:, [0]]
        adjusted_basis -= 1
        adjusted_basis *= 100
        adjusted_basis = adjusted_basis.T
        names = ["basis", "dividend adjustment", "adjusted basis"]
        days_till_last_expiration = (expirations1[-1] - index[0]).days
        dates = [index[0] + dt.timedelta(days=i) for i in range(days_till_last_expiration+1)]
        data = np.zeros((days_till_last_expiration+1, 3))
        data[0, :] = np.hstack((0, adjusted_basis[0, :]))
        i = 0
        for j, d in enumerate(dates):
            if d == expirations1[i]:
                data[j, :] = np.hstack((j, adjusted_basis[i+1, :]))
                i += 1
            elif j!= 0:
                data[j, :] = None
            else:
                pass
        divi_points=pd.DataFrame(dividends_adj,columns=['%s'%ticker,'near month','far month','near season','far season'])
        divi_points['Date']=0
        divi_points['Date'][0]='today'
        divi_points["Date"][1]='yesterday'
        divi_points=divi_points.set_index('Date')
        divi_points.to_excel('Z:/Data/dividend/%s_%s.xlsx'%(ticker,date))
        output[ticker] = pd.DataFrame(data = data, columns = np.hstack(("Days Till Expirations", index)), index = dates)

    return output


#First we make configurations for SQLServer connection

server = "192.168.1.6"
user = "hang.he"
password = "jd123456"
directory = "Z:\Morning_Report"#The directory where you want to put the files
conn = pymssql.connect(server=server, user=user, password=password, database='JYDB')
cursor = conn.cursor()
    

#First we make the plot for IF term structure
print("Making term structure plots...")
date = dt.datetime.now().date()
directory = r"Z:\Morning_Report\daily_plots\%s"%date
if not os.path.exists(directory):
    os.makedirs(directory)
ts = make_report(date, conn)
for ticker in ("IF", "IH", "IC"):
    term_structure = ts[ticker]
    mask = np.logical_not(np.isnan(term_structure.ix[:, 1]))
    mask = np.where(mask)[0]
    if sum(mask) !=5 :
        term_structure.ix[mask[0], 1] = 0
        term_structure.ix[mask[0], 2] = 0
    fig = plt.figure(figsize=(15, 8))
    plt.xticks(term_structure["Days Till Expirations"][mask])
    plt.tick_params(axis='x', which='major', labelsize=18)
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(term_structure["Days Till Expirations"][mask], term_structure.ix[mask, 1], 
        label=term_structure.columns[1], marker=".", markersize=15)
    """for i, j in zip(term_structure["Days Till Expirations"][mask], term_structure.ix[mask, 1]):
            ax.annotate("%.2f%%"%j, xy=(i-2, j), size=20)"""

    mask = np.logical_not(np.isnan(term_structure.ix[:, 2]))
    ax.plot(term_structure["Days Till Expirations"][mask], term_structure.ix[mask, 2], 
        label=term_structure.columns[2], marker=".", markersize=15)
    """for i, j in zip(term_structure["Days Till Expirations"][mask], term_structure.ix[mask, 2]):
        ax.annotate("%.2f%%"%j, xy=(i+2, j), size=20)"""
    mask = np.where(np.logical_not(np.isnan(term_structure.ix[:, 1])))[0]

        
    ref_l = abs(term_structure.ix[mask[0], 1] - term_structure.ix[mask[0], 2])
    ref_l = [0 if ref_l > 0.2 else 0.2]
    if len(mask) == 5:
        ax.annotate("0.00", xy=(-mask[1]/2, 0), size=20)
    elif term_structure.ix[mask[0], 1] >= term_structure.ix[mask[0], 2]:
        ax.annotate("%.2f%%"%term_structure.ix[mask[0], 1], xy=(mask[0], term_structure.ix[mask[0], 1]), size=20)
        ax.annotate("%.2f%%"%term_structure.ix[mask[0], 2], xy=(mask[0], term_structure.ix[mask[0], 2]), size=20)
    elif term_structure.ix[mask[1], 1] < term_structure.ix[mask[1], 2]:
        ax.annotate("%.2f%%"%term_structure.ix[mask[0], 1], xy=(mask[0], term_structure.ix[mask[0], 1]), size=20)
        ax.annotate("%.2f%%"%term_structure.ix[mask[0], 2], xy=(mask[0], term_structure.ix[mask[0], 2]), size=20)
            
    if term_structure.ix[mask[1], 1] >= term_structure.ix[mask[1], 2]:
        ax.annotate("%.2f%%"%term_structure.ix[mask[1], 1], xy=(mask[1], term_structure.ix[mask[1], 1]), size=20)
        ax.annotate("%.2f%%"%term_structure.ix[mask[1], 2], xy=(mask[1], term_structure.ix[mask[1], 2]), size=20)
    elif term_structure.ix[mask[1], 1] < term_structure.ix[mask[1], 2]:
        ax.annotate("%.2f%%"%term_structure.ix[mask[1], 1], xy=(mask[1], term_structure.ix[mask[1], 1]), size=20)
        ax.annotate("%.2f%%"%term_structure.ix[mask[1], 2], xy=(mask[1], term_structure.ix[mask[1], 2]), size=20)
    for i in mask[2:]:
        ref_l = abs(term_structure.ix[i, 1] - term_structure.ix[i, 2])
        ref_l = [0 if ref_l > 0.2 else 0.2]
        if term_structure.ix[i, 1] >= term_structure.ix[i, 2]:
            ax.annotate("%.2f%%"%term_structure.ix[i, 1], xy=(i, term_structure.ix[i, 1]), size=20)
            ax.annotate("%.2f%%"%term_structure.ix[i, 2], xy=(i, term_structure.ix[i, 2]), size=20)
        elif term_structure.ix[i, 1] < term_structure.ix[i, 2]:
            ax.annotate("%.2f%%"%term_structure.ix[i, 1], xy=(i, term_structure.ix[i, 1]), size=20)
            ax.annotate("%.2f%%"%term_structure.ix[i, 2], xy=(i, term_structure.ix[i, 2]), size=20)
        else:
            ax.annotate("%.2f%%"%term_structure.ix[i, 1], xy=(i, term_structure.ix[i, 1]), size=20)

            
        #Get SHIBOR 1M rate
    query = """
        SELECT [ID]
              ,[TradingDay]
              ,[OfferedMarket]
              ,[OfferedMaturity]
              ,[OfferedMoney]
              ,[WeightAveragePrice]
              ,[UpdateTime]
              ,[JSID]
              ,[InnerCode]
        FROM [JYDB].[dbo].[Bond_OtherIBOR]
        WHERE TradingDay = '%s' and OfferedMarket = 6 and OfferedMaturity=10"""%term_structure.columns[1]
    shibor = pd.read_sql(query, conn)
    shibor = shibor["WeightAveragePrice"][0]

    if term_structure.ix[i, 1] > 0:
        ax.legend(loc = 4, prop={"size": 20})
    else:
        ax.legend(loc = 1, prop={"size": 20})
    ax.set_xlim(-5, term_structure["Days Till Expirations"][mask][-1]+5)
    loc = ax.get_ylim()
    ps = [loc[0], loc[1], term_structure.ix[mask[-1], 1], term_structure.ix[mask[-1], 2]]
    ps = sorted(ps)
    di = [ps[i+1]-ps[i] for i in range(3)]
    st = (ps[di.index(max(di))] + ps[di.index(max(di))+1])/2

    ax.text(term_structure["Days Till Expirations"][mask][-1]-20, st, "SHIBOR 1M\n%s"%shibor,
            bbox = {"boxstyle":"square, pad=0.3", "fc":"white"}, size=20)
    plt.savefig(r"%s\%s_term_structure_%s.png"%(directory, ticker, term_structure.columns[1]), bbox_inches='tight')



